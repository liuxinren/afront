/*
Copyright 2012 Jerome Robert


This file is part of Afront.

Afront is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Afront is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA
 */

#include "AnalyticGuidanceField.h"
#include <fstream>
using namespace std;
/**
 * if the distance from the source is lower than this sqrt(sqrD0) the
 * guidance size is size0. If the distance is greater than this
 * sqrt(sqrD1) the guidance size is AnalyticGuidanceField::step
 */
class Source
{
public:
    virtual void read(istream & in)
    {
        in.read((char*)&size0, sizeof(double));
        in.read((char*)&sqrD0, sizeof(double));
        in.read((char*)&sqrD1, sizeof(double));
        delta = sqrD1 - sqrD0;
        factor = sqrD0 / delta;
    }
    double targetSize(const Point3 &point, double sizeInf)
    {
        double d2 = sqrDistance(point);
        double v;
        if(d2 < sqrD0)
            v = size0;
        else if(d2 < sqrD1)
        {
            double deltaS = sizeInf - size0;
            v = deltaS * d2 / delta + (size0 - factor * deltaS);
        }
        else
            v = sizeInf;
        return v;
    }
protected:
    virtual double sqrDistance(const Point3 &point)=0;
    void readPoint(Point3 & point, istream & in)
    {
        for(int i = 0; i < 3; i++)
            in.read((char*)&(point[i]), sizeof(double));
    }

private:
    double sqrD0;
    double sqrD1;
    double size0;
    double delta, factor;
};

class Line: public Source
{
public:
    gtb::tSegment3<double> segment;
    Line(istream & in)
    {
        read(in);
    }

    double readDouble(istream & in)
    {
        double v;
        in.read((char*)&v, sizeof(double));
        return v;
    }

    void read(istream & in)
    {
        Source::read(in);
        Point3 p1, p2;
        readPoint(p1, in);
        readPoint(p2, in);
        segment = gtb::tSegment3<double>(p1, p2);
    }

    double sqrDistance(const Point3 &point)
    {
        return segment.squared_distance(point);
    }
};

class Point: public Source
{
public:
    Point3 point;
    Point(istream & in)
    {
        read(in);
    }
    void read(istream & in)
    {
        Source::read(in);
        readPoint(point, in);
    }

    double sqrDistance(const Point3 &p)
    {
        return Point3::squared_distance(point, p);
    }
};

struct Context
{
    bool done;
    const Point3 & point;
    Context(const Point3 & p): done(false), point(p){}
};

AnalyticGuidanceField::~AnalyticGuidanceField()
{
}

AnalyticGuidanceField::AnalyticGuidanceField(real_type _rho, real_type _step, real_type _reduction):
    GuidanceField(_rho, 0, std::numeric_limits<double>::max(), _reduction), step(_step)
{
}

void* AnalyticGuidanceField::OrderedPointTraverseStart(const Point3 &p)
{
    return new Context(p);
}

bool AnalyticGuidanceField::OrderedPointTraverseNext(
	void *ctx, real_type &squared_dist, Point3 &point, real_type &ideal)
{
    Context * context = (Context*)ctx;
    if(context->done)
		return false;
    context->done = true;
    ideal = step;
    int n  = sources.size();
    for (int i = 0; i < n; i++) {
        Source * s = sources[i];
        ideal = min(ideal, s->targetSize(context->point, step));
    }
    squared_dist = ideal * ideal;
	return true;
}

void AnalyticGuidanceField::OrderedPointTraverseEnd(void *ctx)
{
    delete (Context*)ctx;
}

void AnalyticGuidanceField::readMetricFile(char * fileName)
{
    ifstream in(fileName, ios::in | ios::binary);
    int number;
    in.read((char*)&number, sizeof(int));
    for(int i = 0; i < number; i++)
    {
        sources.push_back(new Point(in));
    }
    in.read((char*)&number, sizeof(int));
    for(int i = 0; i < number; i++)
    {
        sources.push_back(new Line(in));
    }
    in.close();
}
